import "../styles/image.css";
import logo from "../images/logo-js-big.png";
import { Row, Col } from "react-bootstrap";

const Image = ({ ...props }) => {
  const { nameFromParent } = props;
  return (
    <div>
      <Row>
        <Col>
          <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.pngfind.com%2Fpngs%2Fm%2F685-6854970_react-logo-png-png-download-logo-png-reactjs.png&f=1&nofb=1" alt="logo" className="image-small" />
        </Col>
        <Col>
          <img src={logo} alt="logo js" className="image-small" />
        </Col>
        <p className="image-paragraph">This is a React Logo</p>
        <p className="image-paragraph">{nameFromParent} - this is from parent</p>
      </Row>
    </div>
  );
};

export default Image;

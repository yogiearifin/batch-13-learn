const ShowName = ({ ...props }) => {
  const { name, age } = props;
  return (
    <div>
      <h1>Hello my name is {name}</h1>
      <h2>and I am {age} years old</h2>
    </div>
  );
};

export default ShowName;

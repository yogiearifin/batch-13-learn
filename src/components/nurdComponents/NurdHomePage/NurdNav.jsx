import { Navbar,Container,Nav,NavDropdown, Image } from "react-bootstrap"

export const NurdNav = () =>{
    return(
        <div>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="/"><Image
                className="nurd-logo"
                src="https://yt3.ggpht.com/Lvd0_MvDba43kJn4RBEk5DhSaOzJ9JFSPx6NGWmCp67POYR8Q3tiHsUeca0CT2Aru-XVj0j3XI8=s88-c-k-c0x00ffffff-no-rj"
                alt="nurdien-logo" roundedCircle
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">    
              </Nav>
              <Nav>
                <Nav.Link href="#about">About Me</Nav.Link>
                <Nav.Link href="#skills">My Skills</Nav.Link>
                <NavDropdown title="Portofolio" id="collasible-nav-dropdown">
                  <NavDropdown.Item href="/nurdien/dataPhoto">Data Photo</NavDropdown.Item>
                  <NavDropdown.Item href="/nurdien/postHero">Post Hero</NavDropdown.Item>
                  <NavDropdown.Item href="/nurdien/tmdb">TMDB</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Contact" id="collasible-nav-dropdown">
                  <NavDropdown.Item href="https://www.linkedin.com/in/nurdienadijaya/" target="_blank">LinkedIn</NavDropdown.Item>
                  <NavDropdown.Item href="https://www.instagram.com/nurdienadijaya/" target="_blank">Instagram</NavDropdown.Item>
                  <NavDropdown.Item href="https://www.youtube.com/channel/UCwsgYSgdN0KXhRo82EO4bvg" target="_blank">Youtube</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#">Certificate</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=nurdienadijaya@gmail.com" target="_blank">Hire Now!</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        </div>
    );
};

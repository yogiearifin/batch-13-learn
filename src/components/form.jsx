import "./form.css";
import "../App.css";
import { Button } from "react-bootstrap";

// import must be declared above component declarationg

// 2 main rule of components
// 1.single responsibility -> only fullfil one main function -> form.jsx -> for showing form and showing data about form
// 2. reusability -> a component is better to be reusable on another page -> more than one time use

function Form({ ...props }) {
  // component declaration
  // function or js object must be coded below component declaration and above return
  const { nameFromParent, plusFunction, arrayParent, objectParent } = props; // destructuring props
  console.log("array", arrayParent);
  console.log("object", objectParent);
  return (
    // visual must be coded below return
    <div>
      <h1>This is Form</h1>
      {/* self close tag -> /> */}
      <input type="text" placeholder="username" />
      <input type="password" placeholder="password" />
      <Button className="btn-submit">Submit</Button>
      <Button className="btn-app">Button from App </Button>
      <h1>My name is {nameFromParent}</h1>
      <h2>The result of 2 + 2 is {plusFunction(2, 2)}</h2>
      <h2>
        This is a data from object: {objectParent.name} - {objectParent.age}
      </h2>
      <a href="/new">
        <Button>Click Me To Redirect To New Page</Button>
      </a>
    </div>
  );
}
export default Form;

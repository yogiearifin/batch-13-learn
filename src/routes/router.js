import { Switch, Route } from "react-router-dom";

import Main from "../pages/main";
import NewPage from "../pages/newPage";
import Yogie from "../pages/yogie/yogie";
import Me from "../pages/Balya/me";
import "bootstrap/dist/css/bootstrap.css";
import Suto from "../pages/suto/suto";
import Nurdien from "../pages/nurdien/nurdien";
import PostYogie from "../pages/yogie/postYogie";
import PostHeroNurdien from "../pages/nurdien/postHeroNurdien";
import DataPhotoNurdien from "../pages/nurdien/dataPhoto";
import NurdTmdb from "../pages/nurdien/NurdTmdb";
import NurdTmdbDetail from "../pages/nurdien/NurdTmdbDetail";
import ServerTest from "../pages/jsonServerTest/serverTest";
import TestReducer from "../pages/yogie/reducer";
import ContextPage from "../pages/contextPage/Context";
import Bima from "../pages/bima/bima"

const Routers = () => {
  return (
    // switch -> just like switch case where it read routes/ browser url and return it accordingly
    <Switch>
      {/* exact means the path must be the same as the value  */}
      <Route exact path="/">
        {/* return this component if the router is root aka / */}
        <Main />
      </Route>
      <Route exact path="/new">
        <NewPage />
      </Route>
      <Route exact path="/yogie">
        <Yogie />
      </Route>
      <Route exact path="/balya">
        <Me />
      </Route>
      <Route exact path="/suto">
        <Suto />
      </Route>
      <Route exact path="/nurdien">
        <Nurdien />
      </Route>
      <Route exact path="/yogie/post">
        <PostYogie />
      </Route>
      <Route exact path="/nurdien/dataPhoto">
        <DataPhotoNurdien />
      </Route>
      <Route exact path="/nurdien/postHero">
        <PostHeroNurdien />
      </Route>
      <Route exact path="/nurdien/tmdb">
        <NurdTmdb />
      </Route>
      <Route exact path="/nurdien/tmdb/movieDetail/:id">
        <NurdTmdbDetail />
      </Route>
      <Route exact path="/yogie/test-reducer">
        <TestReducer />
      </Route>
      <Route exact path="/yogie/test-server">
        <ServerTest />
      </Route>
      <Route exact path="/context">
        <ContextPage />
        </Route>
      <Route exact path="/bima">
        <Bima />
      </Route>
      {/*  * is for all routes that is not registered in here */}
      <Route path="*">
        <div>
          <h1>NOT FOUND :(</h1>
        </div>
      </Route>
    </Switch>
  );
};

export default Routers;

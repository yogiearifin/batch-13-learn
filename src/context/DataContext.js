import { createContext, useState, useEffect } from "react";
import axios from "axios";

export const DataContext = createContext();

const initialState = [
  {
    id: 1,
    name: "Yogie",
    stack: "Front End",
  },
  {
    id: 2,
    name: "Suto",
    stack: "Mobile",
  },
  {
    id: 3,
    name: "Balya",
    stack: "Back End",
  },
  {
    id: 4,
    name: "Bima",
    stack: "Scrum Master",
  },
  {
    id: 5,
    name: "Nurdien",
    stack: "Project Manager",
  },
];

export const DataProvider = (props) => {
  const [team, setTeam] = useState(initialState);
  const getTeamMember = async () => {
    await axios
      .get("http://localhost:4000/team-member")
      .then((res) => setTeam(res.data))
      .catch((err) => console.log(err));
  };
  const postTeamMember = async (member) => {
    await axios
      .post("http://localhost:4000/team-member", member)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };
  useEffect(() => {
    getTeamMember();
  }, []);
  const addTeam = async (member) => {
    await postTeamMember({
      id: team.length + 1,
      name: member.name,
      stack: member.stack,
    });
    await getTeamMember();
  };
  return (
    <DataContext.Provider value={{ team, addTeam }}>
      {props.children}
    </DataContext.Provider>
  );
};

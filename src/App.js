import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter } from "react-router-dom";
import Routers from "./routes/router";

function App() {
  return (
    // BrowserRouter is for reading your routing
    <BrowserRouter>
    {/* the routing components */}
      <Routers /> 
    </BrowserRouter>
  );
}

export default App;

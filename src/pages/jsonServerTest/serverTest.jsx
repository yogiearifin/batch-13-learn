import { useState, useEffect } from "react";
import axios from "axios";

const ServerTest = () => {
  const [data, setData] = useState([]);
  const [hero, setHero] = useState([]);
  const [update, setUpdate] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const getData = async () => {
    await fetch("http://localhost:4000/dbz-villain")
      .then((response) => response.json())
      .then((convert) => setData(convert));
    await fetch("http://localhost:4000/dbz-hero")
      .then((response) => response.json())
      .then((convert) => setHero(convert));
  };
  console.log("data", data);

  useEffect(() => {
    getData();
  }, []);

  const [allInput, setInput] = useState({
    id: 0,
    name: "",
    power: 0,
  });

  console.log("all input", allInput);

  const handleInput = (e) => {
    setInput({ ...allInput, [e.target.name]: e.target.value });
  };

  const postData = async () => {
    if (allInput.id && allInput.name && allInput.power) {
      await axios
        .post("http://localhost:4000/dbz-hero", allInput)
        .then((res) => console.log(res))
        .catch((err) => console.log(err));
      await getData();
    } else {
      alert("please fill all form");
    }
  };

  // edit button on map
  const handleEdit = (item) => {
    setInput({ id: item.id, name: item.name, power: item.power });
    setIsEdit(true);
  };

  // edit data to api -> button submit
  const editData = async (id) => {
    await axios
      .put(
        `http://localhost:4000/dbz-hero/${id}`,
        // body
        allInput
      )
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    await getData();
    setIsEdit(false);
    setInput({ id: 0, name: "", power: 0 });
  };

  //   useEffect(() => {
  //     getData();
  //     console.log("component updated");
  //   }, [update]);

  const deleteHandle = async (id) => {
    await axios
      .delete(`http://localhost:4000/dbz-hero/${id}`)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    await getData();
  };

  const cancelEdit = () => {
    setIsEdit(false);
    setInput({ id: 0, name: "", power: 0 });
  };

  return (
    <div>
      <h1>Server Test</h1>
      {/* <button onClick={() => setUpdate(!update)}>Update Me</button> */}
      <a href="/yogie/test-reducer">
        <button>Test Reducer Page</button>
      </a>
      <ul>
        {data.map((item, index) => {
          return (
            <li key={index}>
              <ol>
                <li>name: {item.name}</li>
                <li>power: {item.power}</li>
              </ol>
            </li>
          );
        })}
      </ul>
      <ul>
        {hero.map((item, index) => {
          return (
            <li key={index}>
              <ol>
                <li>name: {item.name}</li>
                <li>power: {item.power}</li>
                <button onClick={() => deleteHandle(item.id)}>Delete</button>
                <button onClick={() => handleEdit(item)}>Edit</button>
              </ol>
            </li>
          );
        })}
      </ul>
      <div>
        {/* conditional rendering using ternary operator -> ? */}
        <h1>{isEdit ? "Edit Data" : "Post Data"}</h1>
        <label>ID</label>
        <input
          type="number"
          onChange={(event) => handleInput(event)}
          name="id"
          value={allInput.id}
        />
        <label>Name</label>
        <input
          type="text"
          placeholder="name"
          onChange={(event) => handleInput(event)}
          name="name"
          value={allInput.name}
        />
        <label>Power</label>
        <input
          type="number"
          onChange={(event) => handleInput(event)}
          name="power"
          value={allInput.power}
        />
        <button onClick={isEdit ? () => editData(allInput.id) : postData}>
          {isEdit ? "Edit" : "Submit"}
        </button>
        {isEdit ? <button onClick={cancelEdit}>cancel edit</button> : null}
      </div>
    </div>
  );
};

export default ServerTest;

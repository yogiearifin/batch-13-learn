import "bootstrap/dist/css/bootstrap.css";
import
{ Navbar ,
  Nav,
  Container,
  Row,
  Col,
  Card,
  Form,
  Button,
} 
from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import CardGroup from 'react-bootstrap/CardGroup';
import dayjs from "dayjs";
import berenang from "./Image/berenang.jpg"
import watchmovie from "./Image/watchmovie.jpg"
import camping from "./Image/camping.jpg"
import html from "./Image/html.png"
import css from "./Image/css.jpg"
import javascript from "./Image/js.jpg"

const now = dayjs();
console.log(
    "unix time to now",
    dayjs.unix(1626163210).format("dddd,-DD-MMMM-YYYY")
  );


const bima = () => {
  return (
    <div className="bima-container">
    <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="/">Profile</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="#home">Home Page</Nav.Link>
      <Nav.Link href="#About-Me">About Me</Nav.Link>
      <Nav.Link href="#Contact">Contact</Nav.Link>
    </Nav>
  </Navbar>
        <div>
      <Container className="AboutMe justify-content-md-center  mx-5 my-5">
          <Col>
      <h1 id="home">Hello, My name is Bima</h1>
      <p>Today is {now.format("dddd, DD-MMMM-YYYY")}</p>
      </Col>
      <Col className ="Isian">
      <paragraf>
          Saya sekarang tinggal di jogja, dan sedang belajar di Glints Academy,
          sudah hampir 1 bulan saya belajar sebagai front-end-developer,
          banyak yang bisa saya pelajari di bootcamp,
          sebuah kebanggaan tersendiri dapat membuat web,
          ya akhirnya saya bisa memiliki suatu hal yang dibuat sendiri.
      </paragraf>
      </Col>
      </Container>
      </div>

      <Container>
  <Row className="justify-content-md-center row align-items-center">
    <Col>
    <h2 id="About-Me"> Hobby</h2>
    <p>
      Setiap orang memiliki hobi,
      hobby bisa di gunakan untuk menikmati hidup.
    </p>
    </Col>
    <Col>
    <div className="slider mx-5 my-5">
      <Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={berenang}
      alt="First slide"
    />
    <Carousel.Caption>
      <h3>Berenang</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src= {watchmovie}
      alt="Second slide"
    />

    <Carousel.Caption>
      <h3>Nonton Movie</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src= {camping}
      alt="Third slide"
    />
    <Carousel.Caption>
      <h3>Camping</h3>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
      </div>
    </Col>
  </Row>
</Container>

<Col claasName = "justify-content-md-center row align-items-center">
    <h2>
      Skill
    </h2>
  </Col>
<CardGroup>
  <Card className1 = "mx-2 my-2">
    <Card.Img variant="top" src= {html}/>
    <Card.Body>
      <Card.Title>HTML</Card.Title>
      <Card.Text>
      adalah bahasa markah standar untuk dokumen yang dirancang untuk ditampilkan di peramban internet.
      </Card.Text>
    </Card.Body>
  </Card>
  
  <Card className2 = "mx-2 my-2">
    <Card.Img variant="top" src= {css} />
    <Card.Body>
      <Card.Title>CSS</Card.Title>.
      <Card.Text>
      merupakan bahasa pemrograman yang menjadikan website Anda lebih hidup dan menarik.
      </Card.Text>
    </Card.Body>
  </Card>

  <Card className3 = "mx-2 my-2">
    <Card.Img variant="top" src= {javascript} />
    <Card.Body>
      <Card.Title>Javascript</Card.Title>
      <Card.Text>
      merupakan bahasa pemrograman yang menjadikan website Anda lebih hidup dan menarik.
      </Card.Text>
    </Card.Body>
  </Card>
</CardGroup>


<Form>
  <div id="Contact">
    <h2>
      Contac Me
    </h2>
  </div>
  <Col>
    <Form.Group
    className="mb-3 px-5"
    controlId="exampleForm.ControlInput1">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="name@example.com" />
    </Form.Group>
    </Col>
    <Col>
    <Form.Group
     className="mb-3 px-5"
     controlId="exampleForm.ControlTextarea1">
    <Form.Label>Input Your Message</Form.Label>
    <Form.Control as="textarea" rows={3} />
    </Form.Group>
    </Col>
    <Button className="px-5" variant="primary" type="submit">Submit</Button>
 </Form>
 
    </div>
  );
};

export default bima;

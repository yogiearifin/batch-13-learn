const NewPage = () => {
  return (
    <div>
      <h1>I am a new page</h1>
      <a href="/">
        <button>click me to go back home</button>
      </a>
    </div>
  );
};

export default NewPage;

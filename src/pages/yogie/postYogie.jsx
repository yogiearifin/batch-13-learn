import { Container, Row, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import "./yogie.sass";
import "./yogie.scss";
import styles from "./yogie.module.sass";

const PostYogie = () => {
  const [hero, setHero] = useState({
    heroName: "",
    realName: "",
    quirk: "",
    age: 0,
    description: "",
    rating: 0,
    addedBy: "",
  });
  const [allHero, setAllHero] = useState([]);
  const handleInput = (e) => {
    setHero({ ...hero, [e.target.name]: e.target.value });
  };

  const getData = async () => {
    await axios
      .get("https://sheet.best/api/sheets/cfa00985-9973-44c7-a35b-328c102957bb")
      .then((result) => setAllHero(result.data))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    // didMount
    getData();
  }, []);

  useEffect(() => {
    // didUpdate
    console.log("component did update");
  }, [hero]);
  const postData = async () => {
    // asynchrounous function -> step by step
    if (hero.heroName && hero.realName && hero.quirk && hero.age) {
      // await -> wait for this code to finish, then run next code
      await axios
        .post(
          "https://sheet.best/api/sheets/cfa00985-9973-44c7-a35b-328c102957bb",
          hero
        )
        .then((res) => console.log(res))
        .catch((err) => console.log(err));
      // after the first await is done, the below function will run
      await getData();
    } else {
      alert("please make sure to fill all forms");
    }
  };

  console.log(hero);
  return (
    <Container>
      {/* <h1>Post</h1>
      <Row>
        <label>Hero Name</label>
        <input
          type="text"
          placeholder="Hero Name"
          value={hero.heroName}
          name="heroName"
          onChange={(event) => handleInput(event)}
        />
      </Row>
      <Row>
        <label>Real Name</label>
        <input
          type="text"
          placeholder="Real Name"
          value={hero.realName}
          name="realName"
          onChange={(event) => handleInput(event)}
        />
      </Row>
      <Row>
        <label>Quirk</label>
        <input
          type="text"
          placeholder="Quirk"
          value={hero.quirk}
          name="quirk"
          onChange={(event) => handleInput(event)}
        />
      </Row>
      <Row>
        <label>Age</label>
        <input
          type="number"
          value={hero.age}
          name="age"
          onChange={(event) => handleInput(event)}
        />
      </Row>
      <Row>
        <Button onClick={postData}>Post</Button>
      </Row>
      <Row>
        <h1>Data from API</h1>
        <ul>
          {allHero.map((item, index) => {
            return (
              <li key={index}>
                <p>Hero Name: {item.heroName}</p>
                <p>Real Name: {item.realName}</p>
                <p>Quirk: {item.quirk}</p>
                <p>Age: {item.age}</p>
                <p>Description: {item.description}</p>
                <p>Rating: {item.rating}</p>
                <p>Added By: {item.addedBy}</p>
              </li>
            );
          })}
        </ul>
      </Row> */}
      <div className="yogie-big-container">
        <p>paragraph big</p>
        <a href="/yogie/test-server">
          <button>Button</button>
        </a>
        <div className="yogie-small-container">
          <p>paragraph small</p>

          <h1>Small Container</h1>
        </div>
        <div className={styles.divColored}>
          <h1 className={styles["headerOne"]}>I am styled from module</h1>
          <h2>I am also styled</h2>
          <h1>Header One from divColored</h1>
        </div>
      </div>
    </Container>
  );
};

export default PostYogie;

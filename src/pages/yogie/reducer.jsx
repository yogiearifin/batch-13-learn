import { useState, useReducer } from "react";

const TestReducer = () => {
  //   const [state, setState] = useState("");
  //   const initialState = 0; // default value
  //   const reducer = (state, action) => {
  //     // reducuer-> method
  //     switch (action.type) {
  //       case "ADD":
  //         return state + 1;
  //       case "REDUCE":
  //         return state - 1;
  //       case "RESET":
  //         return initialState;
  //       default:
  //         return state;
  //     }
  //   };
  //   // useReducer -> function
  //   const [states, dispatch] = useReducer(reducer, initialState);
  //   // declaration [states / data , function to modify states] = useReducer(method to modify states, states/ data that we want to modify)

  //   return (
  //     <div>
  //       <h1>Test Reducer Page</h1>
  //       <h1>{states}</h1>
  //       <h1>{initialState}</h1>
  //       <br />
  //       <button onClick={() => dispatch({ type: "ADD" })}>+</button>
  //       <button onClick={() => dispatch({ type: "REDUCE" })}>-</button>
  //       <button onClick={() => dispatch({ type: "RESET" })}>RESET</button>
  //     </div>
  //   );

  const [abilities, setAbilities] = useState({
    ability: "",
    idx: 0,
  });

  console.log(abilities);

  const initialStateJoJo = [
    {
      name: "Jonathan",
      isAlive: false,
      ability: ["Hamon", "Sword"],
      gender: "male",
    },
    {
      name: "Joseph",
      isAlive: true,
      ability: ["Hamon", "Gun", "Stand"],
      gender: "male",
    },
    {
      name: "Jotaro",
      isAlive: false,
      ability: ["Stand", "Stop Time"],
      gender: "male",
    },
    {
      name: "Josuke",
      isAlive: true,
      ability: ["Stand", "Heal"],
      gender: "male",
    },
    {
      name: "Giorno",
      isAlive: true,
      ability: ["Stand", "Heal", "Revive", "Requiem"],
      gender: "male",
    },
  ];

  const DEAD = "DEAD";
  const ALIVE = "ALIVE";
  const ADD_ABILITY = "ADD_ABILITY";
  const MALE = "MALE";
  const FEMALE = "FEMALE";

  const reducerJojo = (state, action) => {
    const { type, payload, prevState } = action;
    switch (type) {
      case DEAD:
        return state.map((item) => {
          if (item.name === payload) {
            item.isAlive = false;
          }
          return item;
        });
      case ALIVE:
        return state.map((item) => {
          if (item.name === payload) {
            item.isAlive = true;
          }
          return item;
        });
      case ADD_ABILITY:
        return state.map((item) => {
          if (item.name === payload) {
            item.ability = [...prevState, abilities.ability];
          }
          setAbilities({
            ability: "",
            idx: 0,
          });
          return item;
        });
      case MALE:
        return state.map((item) => {
          if (item.name === payload) {
            item.gender = "male";
          }
          return item;
        });
      case FEMALE:
        return state.map((item) => {
          if (item.name === payload) {
            item.gender = "female";
          }
          return item;
        });

      default:
        alert("error");
    }
  };

  const [stateJojo, dispatchJojo] = useReducer(reducerJojo, initialStateJoJo);

  return (
    <div>
      <h1>JoJo List</h1>
      {stateJojo.map((item, index) => {
        return (
          <div key={index}>
            <p>name: {item.name}</p>
            <p>status : {item.isAlive ? "alive" : "dead"}</p>
            <p>ability:</p>
            <p>gender : {item.gender}</p>
            <ul>
              {item.ability.map((skill, idx) => {
                return <li key={idx}>{skill}</li>;
              })}
            </ul>
            {item.isAlive ? (
              <button
                onClick={() => dispatchJojo({ type: DEAD, payload: item.name })}
              >
                dead
              </button>
            ) : (
              <button
                onClick={() =>
                  dispatchJojo({ type: ALIVE, payload: item.name })
                }
              >
                alive
              </button>
            )}
            <input
              type="text"
              placeholder="add ability"
              onChange={(e) =>
                setAbilities({ ability: e.target.value, idx: index })
              }
              value={abilities.idx === index ? abilities.ability : ""}
            />
            <button
              onClick={() =>
                dispatchJojo({
                  type: ADD_ABILITY,
                  payload: item.name,
                  prevState: item.ability,
                })
              }
            >
              add ability
            </button>
            {item.gender === "male" ? (
              <button
                onClick={() =>
                  dispatchJojo({ type: FEMALE, payload: item.name })
                }
              >
                female
              </button>
            ) : (
              <button
                onClick={() => dispatchJojo({ type: MALE, payload: item.name })}
              >
                male
              </button>
            )}
          </div>
        );
      })}
    </div>
  );
};

export default TestReducer;

import { useState, useEffect } from "react";
import "./yogie.css";
import { Modal, Button, Row, Col } from "react-bootstrap";
import axios from "axios";
import MappingData from "./mappingData";

const Yogie = () => {
  // const [name, setName] = useState("");
  // console.log("name", name);
  // const [age, setAge] = useState(0);
  // console.log("age", age);

  // const inputName = (event) => {
  //   setName(event.target.value);
  // };

  // setup state to store the data from input -> this is to store value from input
  const [biodata, setBiodata] = useState({
    // default data
    name: "",
    age: 0,
  });

  const [data, setData] = useState([]); // state for storing data from API
  const [post, setPost] = useState([]);
  console.log("state data", data);

  // function to fetch url
  const getData = (url) => {
    // fetching url from parameter
    // fetch(url)
    //   .then((result) => result.json()) //result of fetch will be converted to json
    //   .then((convertedData) => {
    //     console.log("this is fetched data user", convertedData); // console.log to see converted data
    //     setData(convertedData); // data will be stored on a data state
    //   });
    axios.get(url).then((res) => setData(res.data));
  };

  const getDataPost = (url) => {
    // fetching url from parameter
    // fetch(url) // fetch -> vanilla method to get data from JS
    //   .then((result) => result.json()) //result of fetch will be converted to json
    //   .then((convertedData) => {
    //     console.log("this is fetched data post", convertedData); // console.log to see converted data
    //     setPost(convertedData); // data will be stored on a data state
    //   });
    axios.get(url).then((res) => setPost(res.data));
  };

  const [todos, setTodos] = useState([]);
  const getTodosData = (url) => {
    axios.get(url).then((res) => setTodos(res.data));
  };

  const [counter, setCounter] = useState(0);

  useEffect(
    () => {
      // didMount -> when page/component is loaded
      console.log("component mounting");
      getData("https://jsonplaceholder.typicode.com/users");
      getDataPost("https://jsonplaceholder.typicode.com/posts");
      getTodosData("https://jsonplaceholder.typicode.com/todos");
    },
    // dependency -> []
    []
  );

  useEffect(() => {
    // didUpdate -> when page/component receive updates from props or state
    console.log("component did update");
  }, [biodata]);

  // useEffect(() => {
  //   // willUnmount -> when page/component is unload
  //   return () => {
  //     console.log("will unmount");
  //   };
  // });

  // state for calling modal
  const [callModal, setCallModal] = useState(false);

  // function for call modal
  const handleCloseModal = () => {
    setCallModal(false);
  };

  // allBio state is for collecting all input value that has been submited
  const [allBio, setAllBio] = useState([]);
  console.log("all bio", allBio);
  useEffect(() => {
    // didUpdate -> when page/component receive updates from props or state
    console.log("component did update on allBio");
  }, [allBio]);

  console.log("bio", biodata);

  // event listener -> everytime the input is change it will recall this function
  const changeInput = (event) => {
    // change state by keeping the old data using spread operator (...objectName) and receive new input data from name and value
    setBiodata({ ...biodata, [event.target.name]: event.target.value });
  };

  // function for moving data from biodata to allBio
  const submitInput = () => {
    console.log(
      `the submitted data were name: ${biodata.name} and age: ${biodata.age}`
    );
    // put biodata state to all bio, while keeping the previous data of allBio
    setAllBio([...allBio, biodata]);
  };

  // inline style on React in a form of syleObject
  const newStyle = {
    color: "blue",
    backgroundColor: "#f4f4f4",
    textDecoration: "line-through",
  };

  return (
    <>
      <Row className="yogie-container">
        <Col>
          <h1>Hello, My name is Yogie</h1>
          <img
            src="https://media-exp1.licdn.com/dms/image/C5103AQEoBb06TdjkUA/profile-displayphoto-shrink_200_200/0/1517399304128?e=1631750400&v=beta&t=BZ31GZmHxwxwBDPpEOiHG-RMk6m0fhLHdH5qQuczP9E"
            alt="foto yogie"
            className="yogie-photo"
          />
          <a href="/">
            <button className="yogie-btn">click me to go back home</button>
          </a>
          <a href="/yogie/post">
            <button className="yogie-btn">click me to go post page</button>
          </a>
        </Col>
        {/* inline style in react also */}
        <Col style={{ maxHeight: "100px" }}>
          <h1>Input</h1>
          <input
            type="text"
            placeholder="name"
            name="name"
            // everytime the input is changed it will be recorded on onChange function
            // onChange={(event) => inputName(event)}
            onChange={(event) => changeInput(event)}
          />
          <input
            type="number"
            placeholder="age"
            name="age"
            // onChange={(event) => setAge(event.target.value)}
            onChange={(event) => changeInput(event)}
          />
          {/* <input type="radio" name="gender" value={true} />
        <label>Male</label>
        <input type="radio" name="gender" value={true} />
        <label>Female</label> */}
          {/* everytime the button is clicked it will run onClick function */}
          <button onClick={submitInput}>Submit</button>
          {/* newstyle is from styleObject above */}
          <div style={newStyle}>
            <ol>
              {/* mapping for showing data from an array */}
              {allBio.map((item, index) => {
                return (
                  <li key={index} onClick={() => setCallModal(true)}>
                    {item.name} - {item.age} - {index}
                  </li>
                );
              })}
              {/* show property is the dependecy of the modal -> 
            (if the dependency is false the the modal will not be show and vice versa) */}
              <Modal show={callModal}>
                <Modal.Dialog>
                  <Modal.Header>
                    <Modal.Title>Modal title</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <p>Modal body text goes here.</p>
                    {/* <input
                    type="text"
                    placeholder="name"
                    name="name"
                    // everytime the input is changed it will be recorded on onChange function
                    // onChange={(event) => inputName(event)}
                    onChange={(event) => changeInput(event)}
                  />
                  <input
                    type="number"
                    placeholder="age"
                    name="age"
                    // onChange={(event) => setAge(event.target.value)}
                    onChange={(event) => changeInput(event)}
                  /> */}
                  </Modal.Body>

                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                      Close
                    </Button>
                    {/* <Button variant="primary" onClick={submitInput}>
                    Save changes
                  </Button> */}
                  </Modal.Footer>
                </Modal.Dialog>
              </Modal>
            </ol>
          </div>
        </Col>
        <Col>
          <h1>Counter</h1>
          <h2>{counter}</h2>
          <button onClick={() => setCounter(counter + 1)}>+</button>
          <button onClick={() => setCounter(counter - 1)}>-</button>
        </Col>
      </Row>
      <Row>
        <h1>Fetched Data</h1>
        {/* map state data to show on the page */}
        <Col>
          <h2>User</h2>
          {/* // map from fetching */}
          <MappingData data={data} />
        </Col>
        <Col>
          <h2>Post</h2>
          <MappingData data={post} />
        </Col>
        <Col>
          <h2>Todos</h2>
          <MappingData data={todos} />
        </Col>
      </Row>
    </>
  );
};

export default Yogie;

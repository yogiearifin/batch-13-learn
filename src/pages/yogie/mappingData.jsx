const MappingData = ({ ...props }) => {
  const { data } = props;
  return data.map((item, index) => {
    return (
      <li key={index}>
        <ol>
          {item.name ? (
            <>
              <li>Name: {item.name}</li>
              <li>Email: {item.email}</li>
              <li>Company Name: {item.company.name}</li>
            </>
          ) : null}
          {item.title ? (
            <>
              <li
                style={
                  item.completed ? { textDecoration: "line-through" } : null
                }
              >
                Title: {item.title}
              </li>
            </>
          ) : null}
          {item.body ? <li>Body: {item.body}</li> : null}
        </ol>
      </li>
    );
  });
};

export default MappingData;

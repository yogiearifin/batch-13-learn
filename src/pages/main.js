import logo from "../logo.svg";
import "../App.css";
import Form from "../components/form";
import Image from "../components/image";
import ShowName from "../components/showName";
import dayjs from "dayjs";

function Main() {
  // App is parent
  // data
  const now = dayjs();
  const name = "Yogie";
  const plus = (num1, num2) => {
    return num1 + num2;
  };
  const obj = {
    name: name,
    age: 24,
  };
  const arr = [1, 2, 3, 4];
  console.log(now);
  console.log(
    "unix time to now",
    dayjs.unix(1626096659).format("DD-MMMM-YYYY")
  );
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <h1>This is a new React App</h1>
    //     <p>today is {now.format("dddd, DD-MMMM-YYYY")}</p>
    //     {/* // Form is child */}
    //     <Form
    //       // props
    //       // alias = {data/function}
    //       nameFromParent={name}
    //       plusFunction={plus}
    //       objectParent={obj}
    //       arrayParent={arr}
    //     />
    //     <Image nameFromParent={name} />
    //     {/* components that is reusable with different data */}
    //     <ShowName name={"Yogie"} age={24} />
    //     <ShowName name={"Bima"} age={24} />
    //   </header>
    // </div>
    <div className="App">
      <h1>List of Glinst Academy Students Front End class Batch #13</h1>
      <ul>
        <li>
          <a href="/yogie">
            <h1>Yogie</h1>
          </a>
        </li>
        <li>
          <a href="/suto">
            <h1>suto</h1>
          </a>
        </li>
        <li>
          <a href="/balya">
            <h1>Balya</h1>
          </a>
        </li>
        <li>
          <a href="/bima">
          <h1>Bima</h1>
          </a>
        </li>
        <li>
          <a href="/nurdien">
            <h1>Nurdien</h1>
          </a>
        </li>
        <li>
          <a href="/context">
            <h1>Context</h1>
          </a>
        </li>
      </ul>
    </div>
  );
}

export default Main;

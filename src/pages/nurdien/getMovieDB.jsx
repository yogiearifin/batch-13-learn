import axios from "axios";
import { useEffect } from "react";
import { useState } from "react"

const DataMovie = () => {
    const [movie, setMovies] = useState ([]);
    console.log("state data", movie);

    const getMovie = async () => {
        await axios.get ("http://localhost:4000/json-movies")
        .then ((result) => setMovies(result.data))
        .catch((error) => console.log(error));
    }

    useEffect (() =>{
        getMovie()
    },[]);

    return (
        <div>
            <h1>Movie lists</h1>
            <ol>
                {movie.map((item, index) =>{
                    return(
                        <li key={index}>
                            <ul>Movie title: {item.title}</ul>
                            <ul>Movie overview: {item.overview}</ul>
                            <ul>Year: {item.year}</ul>
                            <ul>Genre: {item.genre.join(", ")}</ul>
                        </li>
                    )
                })}
            </ol>
        </div>
    )

}

export default DataMovie;
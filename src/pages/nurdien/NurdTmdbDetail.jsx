import axios from "axios";
import { Button } from "bootstrap";
import React, { useEffect, useState } from "react";
import {
  Card,
  Container,
  Form,
  FormControl,
  ListGroup,
  ListGroupItem,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";
import { useParams } from "react-router-dom";
// import { NurdTmdbNav } from '../../components/nurdComponents/NurdTmdb/NurdTmdbNav';

const img500 = "https://image.tmdb.org/t/p/w500";

const NurdTmdbDetail = () => {
  const { id } = useParams();
  const [tmdbMovieDetail, setTmdbMovieDetails] = useState([]);
  console.log("state data", tmdbMovieDetail);

  const getTmdbMovieDetail = async () => {
    await axios
      .get(
        `https://api.themoviedb.org/3/movie/${id}?api_key=a45dc685fd989f0552715bdafc959584`
      )
      .then((result) => setTmdbMovieDetails(result.data))
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getTmdbMovieDetail();
  }, []);
  console.log(tmdbMovieDetail);

  return (
    <div>
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src={`${img500}/${tmdbMovieDetail.backdrop_path}`}
        />
        <Card.Body>
          <Card.Title>{tmdbMovieDetail.original_title}</Card.Title>
          <Card.Text>{tmdbMovieDetail.overview}</Card.Text>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>
            Release date: {tmdbMovieDetail.release_date}
          </ListGroupItem>
          <ListGroupItem>
            Languages : {tmdbMovieDetail.original_language}
          </ListGroupItem>
          <ListGroupItem>Vote : {tmdbMovieDetail.vote_average}</ListGroupItem>
        </ListGroup>
        <Card.Body>
          <Card.Link href={tmdbMovieDetail.homepage} target="_blank">
            Movie Homepage
          </Card.Link>
          <Card.Link href="#">Another Link</Card.Link>
        </Card.Body>
      </Card>
    </div>
  );
};
export default NurdTmdbDetail;

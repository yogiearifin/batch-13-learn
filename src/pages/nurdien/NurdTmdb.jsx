import axios from 'axios';
import React, { Fragment, useEffect, useState } from 'react';
import { Button, Card, Container, Form, FormControl, ListGroup, ListGroupItem, Nav, Navbar, NavDropdown } from 'react-bootstrap';
// import "./nurdBootstrap.css" ;

const img500 = "https://image.tmdb.org/t/p/w500"

const NurdTmdb = () => {
    const [tmdbMovie,setTmdbMovies] = useState([]);
    console.log("state data", tmdbMovie);

    const getTmdbMovie = async () => {
        await axios.get ("https://api.themoviedb.org/3/movie/upcoming?api_key=a45dc685fd989f0552715bdafc959584")
        .then ((result) => setTmdbMovies(result.data.results))
        .catch((error) => console.log(error));
    }

    useEffect(() => {
        getTmdbMovie()
    },[])
    console.log(tmdbMovie);



    return(
        <Fragment>            
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                <Navbar.Brand href="/nurdien/tmdb">
                    <img
                        alt="nurdien-logo" roundedCircle                        
                        src="https://yt3.ggpht.com/Lvd0_MvDba43kJn4RBEk5DhSaOzJ9JFSPx6NGWmCp67POYR8Q3tiHsUeca0CT2Aru-XVj0j3XI8=s88-c-k-c0x00ffffff-no-rj"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"                    
                    />{' '}
                    TMDB
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    <Nav.Link href="#features">Movies</Nav.Link>
                    <Nav.Link href="#pricing">TV Series</Nav.Link>
                    <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Form className="d-flex">
                        <FormControl
                            type="search"
                            placeholder="Search"
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button className="mx-2" variant="outline-light">Search</Button>
                    </Form>
                </Navbar.Collapse>
                </Container>
            </Navbar>
            <div>
                {tmdbMovie.map ((item) => (                        
                    // <Card style={{ width: '18rem' }}>
                    //     <a href={`/nurdien/tmdb/movieDetail/${item.id}`}>
                    //         <Card.Img variant="top" src={`${img500}/${item.poster_path}`} />
                    //     </a>
                    //         <Card.Body>
                    //             <Card.Title>{item.title}</Card.Title>
                    //             <Card.Text>
                    //             {item.overview}
                    //             </Card.Text>
                    //         </Card.Body>
                    //         <ListGroup className="list-group-flush">
                    //             <ListGroupItem>Languages : {item.original_language}</ListGroupItem>
                    //             <ListGroupItem>Release date: {item.release_date}</ListGroupItem>
                    //             <ListGroupItem>Vote average: {item.vote_average}</ListGroupItem>
                    //         </ListGroup>
                    //         {/* <Card.Body>
                    //             <Card.Link href="#">Card Link</Card.Link>
                    //             <Card.Link href="#">Another Link</Card.Link>
                    //         </Card.Body> */}
                    //     </Card>  
                    <Card style={{ width: '18rem' }}>
                        <a href={`/nurdien/tmdb/movieDetail/${item.id}`}>
                            <Card.Img variant="top" src={`${img500}/${item.backdrop_path}`} />
                        </a>
                        <Card.Body>
                            <Card.Title>{item.title}</Card.Title>
                            <Card.Text>
                            {item.overview}
                            </Card.Text>
                            <Button variant="primary" href={`/nurdien/tmdb/movieDetail/${item.id}`}>Movie Detail</Button>
                        </Card.Body>
                    </Card>          
                    ))}
            </div>
        </Fragment>
    )
}

export default NurdTmdb;
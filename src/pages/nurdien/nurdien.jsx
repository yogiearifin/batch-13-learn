
// import "./nurdien.css";
// import {
//   Nav,
//   Navbar,
//   Container,
//   Row,
//   Col,
//   Form,
//   Button,
// } from "react-bootstrap";

// const Nurdien = () => {
//   return (
//     <div className="nurdien-container">
//       <Navbar fluid bg="light" variant="light">
//         <Container>
//           <Navbar.Brand href="/">
//             <img
//               className="nurd-logo"
//               src="https://yt3.ggpht.com/Lvd0_MvDba43kJn4RBEk5DhSaOzJ9JFSPx6NGWmCp67POYR8Q3tiHsUeca0CT2Aru-XVj0j3XI8=s88-c-k-c0x00ffffff-no-rj"
//               alt="nurdien-logo"
//             />
//           </Navbar.Brand>
//           <Nav className="me-auto">
//             <Nav.Link href="#about">About</Nav.Link>
//             <Nav.Link href="#nurd-video">Video</Nav.Link>
//             <Nav.Link href="#reachMe">Reach Me</Nav.Link>
//           </Nav>
//         </Container>
//       </Navbar>
//       <Row>
//         <Col>
//           <img
//             className="nurd-photo"
//             src="https://nurdienadijaya.github.io/assets/100%20Entertainment%20Nurdien.jpg"
//             alt=""
//             width="300em"
//           />
//         </Col>
//         <Col>
//           <h1 id="about" className="nurd-bio">
//             Code, Videograph, and Music
//           </h1>
//           <p>
//             Hi, I'm Nurdien Adijaya. A passionate Front End Developer,
//             Videography and Musician
//             <br />
//             based in the Indonesia, focus on crafting quality.
//           </p>
//           <form>
//             <Form.Group className="mb-3 input-email" controlId="formBasicEmail">
//               <Form.Label>Email address</Form.Label>
//               <Form.Control type="email" placeholder="Enter email" />
//               <Form.Text className="text-muted">
//                 We'll never share your email with anyone else.
//               </Form.Text>
//             </Form.Group>
//             <Button variant="secondary" type="submit">
//               Start to Subscribe
//             </Button>
//             {/* <input type="email" placeholder="yourmail@example.com" id="" />
//             <button>Start to Subscribe</button> */}
//           </form>
//         </Col>
//       </Row>
//       <div id="nurd-video">
//         <h2>My Video</h2>
//         <embed
//           className="video1"
//           src="https://www.youtube.com/embed/cQc9Nk2-d8c"
//           type=""
//         />
//         <embed
//           className="video2"
//           src="https://www.youtube.com/embed/jnOStI2IDws"
//           type=""
//         />
//         {/* <li><iframe width="560" height="315" src="https://www.youtube.com/embed/Ok0MpU6RHxk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></li>
//             <br>
//             <li><iframe width="560" height="315" src="https://www.youtube.com/embed/cQc9Nk2-d8c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></br> */}
//       </div>
//       <div className="footer">
//         <Row>
//           <Col id="reachMe" className="nurd-reachMe">
//             <h5>Reach Me</h5>
//             <Form>
//               <Form.Group
//                 className="mb-3"
//                 controlId="exampleForm.ControlInput1"
//               >
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control type="email" placeholder="name@example.com" />
//               </Form.Group>
//               <Form.Group
//                 className="mb-3"
//                 controlId="exampleForm.ControlTextarea1"
//               >
//                 <Form.Label>Input Your Message</Form.Label>
//                 <Form.Control as="textarea" rows={3} />
//               </Form.Group>
//               <Button variant="primary" type="submit">
//                 Submit
//               </Button>
//             </Form>
//           </Col>
//           <Col className="nurd-sosmed">
//             <a
//               className="nurd-sosmed1"
//               href="https://www.linkedin.com/in/nurdienadijaya/"
//               target="_blank"
//             >
//               <img
//                 src="http://assets.stickpng.com/images/58e91afdeb97430e81906504.png"
//                 alt=""
//                 height="50px"
//               />
//             </a>
//             <a
//               className="nurd-sosmed2"
//               href="https://www.instagram.com/nurdienadijaya/"
//               target="_blank"
//             >
//               <img
//                 src="https://www.freepnglogos.com/uploads/instagram-logo-png-transparent-background-hd-3.png"
//                 alt=""
//                 height="50px"
//               />
//             </a>
//             <a
//               className="nurd-sosmed3"
//               href="https://www.youtube.com/channel/UCwsgYSgdN0KXhRo82EO4bvg"
//               target="_blank"
//             >
//               <img
//                 src="https://logodownload.org/wp-content/uploads/2014/10/youtube-logo-5-2.png"
//                 alt=""
//                 height="50px"
//               />
//             </a>
//             <a
//               className="nurd-sosmed4"
//               href="https://wa.me/6285694971856"
//               target="_blank"
//             >
//               <img
//                 src="https://seeklogo.com/images/W/whatsapp-icon-logo-BDC0A8063B-seeklogo.com.png"
//                 alt=""
//                 height="50px"
//               />
//             </a>
//             <p>&copy; 2021</p>
//           </Col>
//         </Row>
//       </div>
//     </div>
//   );
// };

// export default Nurdien;


///////////////////////////////////////////////////////////////////////////////////////////////////

// import { Fragment } from "react";
// import { NurdNav } from "../../components/nurdComponents/nurdNav";



// const Nurdien = () =>{
//   return(
//     <Fragment>
//       <NurdNav />
//     </Fragment>
//   );
// };

// export default Nurdien;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import { Fragment } from "react";
import "./nurdBootstrap.css" ;
import {
  Navbar,
  Container,
  Nav,
  NavDropdown,
  Image,
  Row,
  Col,
  Form,
  Button,
} from "react-bootstrap";
import DataPhoto from "./dataPhoto";
import DataMovie from "./getMovieDB";


const Nurdien = () =>{
  return(
    <Fragment>

      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/"><Image
              className="nurd-logo"
              src="https://yt3.ggpht.com/Lvd0_MvDba43kJn4RBEk5DhSaOzJ9JFSPx6NGWmCp67POYR8Q3tiHsUeca0CT2Aru-XVj0j3XI8=s88-c-k-c0x00ffffff-no-rj"
              alt="nurdien-logo" roundedCircle
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">    
            </Nav>
            <Nav>
              <Nav.Link href="#about">About Me</Nav.Link>
              <Nav.Link href="#skills">My Skills</Nav.Link>
              <NavDropdown title="Portofolio" id="collasible-nav-dropdown">
                <NavDropdown.Item href="/nurdien/dataPhoto">Data Photo</NavDropdown.Item>
                <NavDropdown.Item href="/nurdien/postHero">Post Hero</NavDropdown.Item>
                <NavDropdown.Item href="/nurdien/tmdb">TMDB</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Contact" id="collasible-nav-dropdown">
                <NavDropdown.Item href="https://www.linkedin.com/in/nurdienadijaya/" target="_blank">LinkedIn</NavDropdown.Item>
                <NavDropdown.Item href="https://www.instagram.com/nurdienadijaya/" target="_blank">Instagram</NavDropdown.Item>
                <NavDropdown.Item href="https://www.youtube.com/channel/UCwsgYSgdN0KXhRo82EO4bvg" target="_blank">Youtube</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#">Certificate</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=nurdienadijaya@gmail.com" target="_blank">Hire Now!</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Container className="content" >
        <Row >
          <Col fluid>
            <Container >
              <Image className="nurdImage"
                src="https://nurdienadijaya.github.io/assets/100%20Entertainment%20Nurdien.jpg"
                alt=""
                width="340em"
              />
            </Container>
          </Col>
          <Col auto id="about" >
            <Container >
              <h1  className="nurd-bio">
                Code, Videograph, and Music
              </h1>
              <p>
                Hi, I'm Nurdien Adijaya. A passionate Front End Developer,
                Videography and Musician
                <br />
                based in the Indonesia, focus on crafting quality.
              </p>
              <form>
                <Form.Group className="mb-3 input-email" controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type="email" placeholder="Enter email" />
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>
                <Button variant="secondary" type="submit">
                  Start to Subscribe
                </Button>
              </form>
           </Container>
          </Col>
        </Row>
      </Container>

      <Container>
        <Row className="justify-content-md-center myVideo">
          <Col xs lg="2">
          </Col>
          <Col md="auto"><h2>My Video</h2></Col>
           <Col xs lg="2">
          </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col>
              <embed 
                className="video1"
                src="https://www.youtube.com/embed/cQc9Nk2-d8c"
                type=""
                width="450rem"
                height="250rem"
              />
          </Col>
          <Col >
              <embed
                className="video2"
                src="https://www.youtube.com/embed/jnOStI2IDws"
                type=""
                width="450rem"
                height="250rem"
              />
          </Col>
        </Row>
      </Container>

      <Navbar bg="dark">
        <h5>Reach Me</h5>
        <Form>
          <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlInput1"
          >
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="name@example.com" />
          </Form.Group>
          <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlTextarea1"
          >
            <Form.Label>Input Your Message</Form.Label>
            <Form.Control as="textarea" rows={3} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Navbar>
      <DataMovie/>
    </Fragment>
  );
};

export default Nurdien;
import axios from "axios";
import { useEffect } from "react";
import { useState } from "react"
import { Row, Col, Container, Button } from "react-bootstrap"


const PostHeroNurdien = () => {
    const [hero, setHero] = useState({
        heroName: "",
        realName: "",
        quirk: "",
        age: 0,
        description: "",
        rating: 0,
        addedBy: "",
    });

    const [allHero, setAllHero]  = useState([]);
    const handleInput = (arr) => {
        setHero({ ...hero, [arr.target.name]: arr.target.value});
    };

    const getData = async () => {
        await axios.get ("https://sheet.best/api/sheets/cfa00985-9973-44c7-a35b-328c102957bb")
        .then((result) => setAllHero(result.data))
        .catch((error) => console.log(error));
    }   

    useEffect(() =>{
        //didMount
        getData();
    },[]
    ); //-> menampilkan data saat pertama buka pagenya

    useEffect(() =>{
    console.log("component did update");
    },[hero]);

    const postData = async () => {
        if (
            hero.heroName &&
            hero.realName &&
            hero.quirk &&
            hero.age &&
            hero.description &&
            hero.rating &&
            hero.addedBy
            ){
            await axios.post(
                "https://sheet.best/api/sheets/cfa00985-9973-44c7-a35b-328c102957bb",
                hero
            )
            .then((result) => console.log(result))
            .catch((error) => console.log(error));
            await getData();
        }else{
            alert("please make sure to fill all forms")
        }
    };

    console.log(hero);

    return (
        <Container>
            <Col>
                <a href="/nurdien">
                    <h6>back</h6> 
                </a>
            </Col>     
            <Col>
                <h1>Post Hero</h1>
            </Col>
            <Row>
                <label>Hero Name</label>
                <input
                    type="text"
                    placeholder="Put Hero Name here"
                    value={hero.heroName}
                    name="heroName"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Real Name</label>
                <input
                    type="text"
                    placeholder="Put Real Name here"
                    value={hero.realName}
                    name="realName"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Quirk</label>
                <input
                    type="text"
                    placeholder="Put Hero Quirk here"
                    value={hero.quirk}
                    name="quirk"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Age</label>
                <input
                    type="number"
                    placeholder="Put Hero Age here"
                    value={hero.age}
                    name="age"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Description</label>
                <input
                    type="text"
                    placeholder="Put Hero Description here"
                    value={hero.description}
                    name="description"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Rating</label>
                <input
                    type="number"
                    max="10"
                    placeholder="Put Hero Rating here"
                    value={hero.rating}
                    name="rating"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <label>Added By</label>
                <input
                    type="text"
                    placeholder="Put your name here"
                    value={hero.addedBy}
                    name="addedBy"
                    onChange={(event) => handleInput(event)}
                />
            </Row>
            <Row>
                <Button onClick={postData}>Send</Button>
            </Row>
            <br />
            <br />
            <h1>Data From API</h1>
            <ul>
                {allHero.map((item,index) => {
                    return (
                        <li key={index}>
                            <p>Hero Name : {item.heroName}</p>
                            <p>Real Name : {item.realName}</p>
                            <p>Quirk : {item.quirk}</p>
                            <p>Age : {item.age}</p>
                            <p>Description : {item.description}</p>
                            <p>Rating : {item.rating}</p>
                            <p>Added By : {item.addedBy}</p>
                        </li>
                    )
                })}
            </ul>

        </Container>
    )
}

export default PostHeroNurdien
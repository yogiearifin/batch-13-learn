import axios from "axios";
import { Fragment, useEffect } from "react";
import { useState } from "react"


const DataPhotoNurdien = () => {
    //* make state for storing data
    const [photo, setPhoto] = useState ([]);
    console.log("state data",photo);

    //* fetch url
    const getPhoto = (url) => {
        axios.get(url).then((res) => setPhoto(res.data));
    };

    //*didMount
    useEffect(
        () => {
            console.log("component mounting");
            getPhoto("https://jsonplaceholder.typicode.com/photos");
        },[]
    );

    //* didUpdate
    useEffect(() => {
        console.log("component did update");
    },[photo])
    
    const photoLists = photo.map((item, index) => {
        return (
            <li key={index}>
                <ul>Photo ID : {item.id}</ul>
                <ul>Photo Title : {item.title}</ul>
                <ul><a href={item.url} target="_blank">Open Photo Here</a></ul>
                <br />
            </li>
        )
    })

    return (
        <Fragment>
            <a href="/nurdien">
                <h6>back</h6> 
            </a>
            <h1>this data photo</h1>
            <form>
                <input type="number" min="1" max={photo.length}/>
                <label htmlFor="filter"> Data(s) to display</label>
            </form>
            <ol>
                {/* {photo.map((item, index) => {
                    return (
                        <li key={index}>
                            <ul>Photo ID : {item.id}</ul>
                            <ul>Photo Title : {item.title}</ul>
                            <ul><a href={item.url} target="_blank">Open Photo Here</a></ul>
                            <br />
                        </li>
                    )
                })} */}
                {photoLists}
            </ol>
        </Fragment>
    )



}


export default DataPhotoNurdien;
import React from "react";
import { Fragment } from "react";
import { NavBalya } from "../../components/NavBalya";
import { Hero } from "../../components/heroBalya";
import { About } from "../../components/aboutBalya";
import { MySkils } from "../../components/BalyaSkils";
import { Contact } from "../../components/ContactBalya";
import { Header } from "../../components/headerBalya";
import "./balyaStyle.css";
import "bootstrap/dist/css/bootstrap.css";
import { Project } from "../../components/projectBalya";
import { Footer } from "../../components/footerBalya";
const Me = () => {
  return (
    <Fragment>
      <Header />
      <NavBalya />
      <Hero />
      <About />
      <MySkils />
      <Project />
      <Contact />
      <Footer />
    </Fragment>
  );
};

export default Me;

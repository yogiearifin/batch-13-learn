import { useContext, useState } from "react";
import { DataContext } from "../../context/DataContext";

const ContextPage = () => {
  const { team, addTeam } = useContext(DataContext);
  const [member, setMember] = useState({
    name: "",
    stack: "",
  });
  const handleInput = (e) => {
    setMember({
      ...member,
      [e.target.name]: e.target.value,
    });
  };
  const addTeamMember = () => {
    addTeam(member);
    setMember({
      name: "",
      stack: "",
    });
  };
  console.log(member);
  console.log("team", team);
  return (
    <div>
      <h1>Context Page</h1>
      <ul>
        {team.map((item, index) => {
          return (
            <li key={index}>
              <p>name: {item.name}</p>
              <p>stack: {item.stack}</p>
            </li>
          );
        })}
      </ul>
      <div>
        <label>Name</label>
        <input
          type="text"
          placeholder="name"
          name="name"
          onChange={(event) => handleInput(event)}
          value={member.name}
        />
        <label>Stack</label>
        <input
          type="text"
          name="stack"
          placeholder="stack"
          onChange={(event) => handleInput(event)}
          value={member.stack}
        />
        <button onClick={addTeamMember}>Add</button>
      </div>
    </div>
  );
};
export default ContextPage;

import "./suto.css";
import React from 'react';
import Carousel from "react-bootstrap/Carousel";
import Nav from "react-bootstrap/Nav";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import 'bootstrap/dist/css/bootstrap.css';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Navbar from 'react-bootstrap/Navbar'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import NavDropdown from 'react-bootstrap/NavDropdown'
// Put any other imports below so that CSS from your
// components takes precedence over default styles.


const suto = () => {
return (

<div>




<Navbar collapseOnSelect sticky="top" expand="lg" bg="dark" variant="dark">
  <Navbar.Brand href="#home">Aulia Sutowijoyo</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="#profile">Profile</Nav.Link>
      <Nav.Link href="#portofolio">Portofolio</Nav.Link>
      <NavDropdown title="Other Excitment" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Achievment</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Colaboration</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Contact</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Partner Page</NavDropdown.Item>
      </NavDropdown>
    </Nav>
   
  </Navbar.Collapse>
</Navbar>

  <Container >
    <Row >
      <Col >
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://i.etsystatic.com/10280753/r/il/d9cd72/2668809132/il_570xN.2668809132_pg8j.jpg" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the bulk of
            the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>

      </Col>
      <Col>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://media-exp1.licdn.com/dms/image/C5103AQFu_cpQn6CeIg/profile-displayphoto-shrink_800_800/0/1551016937364?e=1631750400&v=beta&t=or60UitH106huaZWAdT_qX9uuRS0ktt_E3VZn7vEs5c" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the bulk of
            the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
      </Col>
      <Col>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZmk81LkBJGbgbiHkWXys81rOkhupRppZouRF4OA1qRfZZ1twQXLUj0ZOgwITB3Gr7mMU&usqp=CAU" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the bulk of
            the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
      </Col>
    </Row>
  </Container>
  
    <Carousel>
      <Carousel.Item interval={1000}>
        <img className="d-block w-100" src="https://images.unsplash.com/photo-1511467687858-23d96c32e4ae?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" alt="First slide" />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={500}>
        <img className="d-block w-100" src="https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" />
        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src="https://images.unsplash.com/photo-1593642634367-d91a135587b5?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80" />
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    <a href="/">
    <button className="suto-btn">click me to go back home</button>
  </a>
</div>
);
};

export default suto;